// This file is to represent a 'Playa' object in javascript environment

var client = require('mongodb').MongoClient;
var assert = require('assert');
var uri = 'mongodb://juan:1234567@ds043012.mongolab.com:43012/playazo';

// Constructor
var Playa = function(name, description, comments) {
    this.name        = name;
    this.description = description; 
    this.comments    = comments; 
};

// Member function save
Playa.prototype.save = function() {

    try {
        client.connect(uri,
        	function(err, db) {
                assert.equal(err, null);
                console.log('Conectado a la base de datos');
                db.collection('playas').insertOne({ name : this.name,
                                                    description : this.description,
                                                    comments : this.comments }, function(err, result) {
                                                        assert(err,null);
                                                    });
            db.close();
        });
    } catch (err){
        console.log(err);
        return false; 
    }
    
    return true; 
};

// Member functioon update
Playa.prototype.update = function(attributes) {

    try {
        
        client.connect(uri,
        	function(err, db) {
                assert.equal(err, null);
                console.log('Conectado a la base de datos');
                var playas = db.collection('playas');
                
                playas.update({name : this.name,
                              description: this.description,
                              comments : this.comments},
                              { $set : attributes }, function(err, result){
                                  assert(err, null);
                              });
            db.close();
        });
    } catch (err) {
        console.log(err); 
        return false; 
        
    }
    
    return true; 
};

// Static functoin all
Playa.all = function() {
    var playas_array = [];
    client.connect(uri,
    	function(err, db) {
            assert.equal(err, null);
            console.log('Conectado a la base de datos');
            
            var playas = db.collection('playas');
            playas.find({}).toArray(function(err,playas){
                assert(err, null);
                console.log('Found playas: ');
                console.dir(playas);
            
               
                // Getting the playas into the variable playas_array
                for (var playa in playas) {
                    playas_array.push(new Playa(playa.name, playa.description, playa.comments));
                }
                
            });
        db.close();
    });
    
    return playas_array;
    
};