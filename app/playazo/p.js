// Variables to connect to database
var mongo = require('mongodb');
var client = mongo.MongoClient;
var assert = require('assert');
var uri = 'mongodb://juan:1234567@ds043012.mongolab.com:43012/playazo';

// BEGIN DEFINING PLAYA OBJECTS

// Constructor
var Playa = function(name, description, comments) {
    this.name        = name;
    this.description = description; 
    this.comments    = comments; 
};



// Member function save
Playa.prototype.save = function() {

    console.log('Saving playa with '+this.name+', '+this.description+', '+this.comments);
    var name = this.name;
    var description = this.description; 
    var comments = this.comments; 
    
    
    try {
        client.connect(uri,
        	function(err, db) {
                assert.equal(err, null);
                console.log('Conectado a la base de datos');
                var playas = db.collection('playas');
                playas.insertOne({  name : name,
                                    description : description,
                                    comments : comments }, function(err, result) {
                                        assert(err,null);
                                        console.log('No errors');
                                    });
            db.close();
        });
    } catch (err){
        console.log(err);
        return false; 
    }
    
    return true; 
};

// Member functioon update
Playa.prototype.update = function(attributes, callback) {
    
    var name = this.name;
    var description = this.description;
    var comments = this.comments; 

    try {
        
        client.connect(uri,
        	function(err, db) {
        	    console.log(attributes);
        	    console.log(name);
                console.log(description);
                console.log(comments);
                assert.equal(err, null);
                console.log('Conectado a la base de datos');
                var playas = db.collection('playas');
                
                playas.update({name : name },
                              { $set : attributes }, function(err, result){
                                  assert.equal(err, null);
                                  
                                  db.close();
                                  callback(true); 
                              });
            
        });
    } catch (err) {
        console.log(err); 
        callback(false); 
        
    }
};

// Static functoin all
Playa.all = function(callback) {
    client.connect(uri,
    	function(err, db) {
            assert.equal(err, null);
            console.log('Conectado a la base de datos');
            
            var playas = db.collection('playas');
            playas.find({}).toArray(function(err,playas){
                assert.equal(err, null);
                db.close();
                callback(playas); 
            
            });
        
    });
    
};

// Static function to get a Playa
Playa.get = function(name, callback) {
    
    try {
        client.connect(uri,
        	function(err, db) {
                assert.equal(err, null);
                console.log('Conectado a la base de datos');
                
                var playas = db.collection('playas');
    
                playas.findOne({ 'name' : name }, function(err,result) {
                    assert.equal(err,null);
                    db.close();
                    callback(result);
                });
                
            });
    } catch (err) {
        
        console.log(err);
    }
};
// END DEFINING PLAYA OBJECTS

// BEGIN DEFINING CAMBIO OBJECTS

var Cambio = function(playa) {
    this.playa     = playa;
    this.playaName = playa.name; 
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    this.date = month + "-" + day + "-" + year + " " + d.getHours() +":"+d.getMinutes()+":"+d.getSeconds(); 
};

Cambio.all = function(playaName, callback) {
        client.connect(uri,
    	function(err, db) {
            assert.equal(err, null);
            console.log('Conectado a la base de datos');
            
            var cambios = db.collection('cambios');
            cambios.find({ playaName : playaName }).toArray(function(err,cambios){
                assert.equal(err, null);
                db.close();
                callback(cambios); 
            
            });
        
    });
};

// Member function save
Cambio.prototype.save = function(callback) {
    
    
    var playa = this.playa;
    var date = this.date; 
    var playaName = this.playaName;

    try {
        client.connect(uri,
        	function(err, db) {
                assert.equal(err, null);
                console.log('Conectado a la base de datos');
                var cambios = db.collection('cambios');
                cambios.insertOne({ playa : playa,
                                    date : date,
                                    playaName : playaName }, function(err, result) {
                                        assert.equal(err,null);
                                        console.log('No errors');
                                        db.close();
                                        callback(true);
                                    });
            
        });
    } catch (err){
        console.log(err);
        callback(false); 
    }
};

Cambio.revert = function(cambioId, callback) {
    
    client.connect(uri,
    	function(err, db) {
            assert.equal(err, null);
            console.log('Conectado a la base de datos');
            
            var cambios = db.collection('cambios');
            
            console.log("Cambio ID: "+ cambioId);

            var o_id = new mongo.ObjectID(cambioId);
            // Find cambio
            cambios.findOne({ '_id' : o_id }, function(err,result) {
                assert.equal(err,null);
                assert.notEqual(result,null);
           
                // Find playa related to cambio
                Playa.get(result.playa.name, function(object){
                    var playa = new Playa(object.name, object.description, object.comments);
                    var dict = { name : result.playa.name,
                                 description : result.playa.description,
                                 comments : result.playa.comments
                                };
                    // Update playa to revert cambio
                    playa.update(dict, function(updated){
                        console.log(updated);
                        if (updated) {
                            console.log("Reverted");
                            callback(true);
                        } else {
                            console.log("Not reverted");
                            callback(false);
                        }
                    });
                });
            
            });
    });
};

// END DEFINING CAMBIO OBJECTS

module.exports = function (app) {
    var module = {};



app.post('/p/ACrearPlaya', function(request, response) {
    console.log('Calling ACrearPlaya');
    //POST/PUT parameters
    var params = request.body;
    
    var results = [{'label':'/VPlaya/'+params['nombre'], 'msg':['Playa {{playa.nombre}} creada']}, {'label':'/VCrearPlaya', 'msg':['Error al tratar de crear playa']}, ];
    var res = results[0]; 
    (function ($next) {
        
    var playa = new Playa(params['nombre'], params['descripcion'], params['comentario']);
    if (playa.save()) {
        res = results[0];
    } else  {
        res = results[1]; 
    }
    
    
    $next(); 

    })(function(){response.json(res);});

});
app.post('/p/AModifPlaya', function(request, response) {
    console.log('Calling AModifyPlaya');
    //POST/PUT parameters
    var params = request.body;
    
    var results = [{'label':'/VPlaya/'+params.name, 'msg':['Playa {{playa.nombre}} modificada.']}, {'label':'/VModifPlaya', 'msg':['Error al tratar de modificar playa']}, ];
    var res;
    (function ($next) {
        Playa.get(params.name, function(object){
            var playa = new Playa(object.name, object.description, object.comments);
            console.log(playa);
            delete params._id; 
            playa.update(params, function(updated){
                console.log(updated);
                if (updated) {
                    res = results[0];
                } else {
                    res = results[1];
                }
                
                var cambio = new Cambio(playa);
                cambio.save(function(saved){
                    console.log(saved);
                    $next();    
                });
                
            });
        });
        
    })(function(){response.json(res);});

});
app.get('/p/ARevertir', function(request, response) {
    //GET parameter
    console.log('Calling ARevertir');
    var idCambio = request.query['id'];
    console.log(request.query);
    var results = [{'label':'/VPlayas', 'msg':['Último cambio revertido']}, {'label':'/VCambios', 'msg':['Error al tratar de revertir un cambio en  playa']}, ];
    var res;
    (function ($next) {
        
        Cambio.revert(idCambio, function(reverted){
            if (reverted) {
                console.log("Reverted");
                res = results[0];
            } else {
                res = results[1];
            }
            $next();
        });
    //Action code ends here
    })(function(){response.json(res);});

});
app.get('/p/VCambios', function(request, response) {
    //GET parameter
    console.log('Calling VCambios');
    var idPlaya = request.query['idPlaya'];
    var res = {};
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished

        Cambio.all(idPlaya, function(object) {
            res['data0'] = object;
            console.log('----------------------');
            console.log(res);
            $next();
        });
    
    })(function(){response.json(res);});

});
app.get('/p/VCrearPlaya', function(request, response) {
    console.log('Calling VCrearPlaya');
    var res = {};
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished


    //Action code ends here
    })(function(){response.json(res);});

});
app.get('/p/VModifPlaya', function(request, response) {
    console.log('Calling VModifyPlaya');
    //GET parameter
    var idPlaya = request.query['idPlaya'];
    var res = {};
    (function ($next) {
        
        Playa.get(idPlaya, function(object){
            res['fPlaya'] = object;
            $next();
        });

    //Action code ends here
    })(function(){response.json(res);});

});
app.get('/p/VPlaya', function(request, response) {
    //GET parameter
    console.log('Calling VPlaya');
    var idPlaya = request.query['idPlaya'];
    var res = {};
    (function ($next) {
        
        Playa.get(idPlaya, function(object){
            res['playa'] = object;
            $next();
        });
        
    })(function(){response.json(res);});

});
app.get('/p/VPlayas', function(request, response) {
    var res = {};
    console.log('Calling VPlayas');
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished

        Playa.all(function(object) {
            res['data0'] = object;
            console.log('----------------------');
            console.log(res);
            $next();
        });
    
    })(function(){response.json(res);});

});

//Use case code starts here


//Use case code ends here

    return module;

};

