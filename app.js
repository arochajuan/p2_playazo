var express = require('express');
var bodyParser = require('body-parser')
var session = require('express-session');
var app = express();

//Other middleware code starts here
//Other middleware code ends here

app.use(bodyParser.json());
app.use(express.static('static'));
app.use(session({
  secret: Math.random()+'',
  resave: false,
  saveUninitialized: true
}))

//Application code starts here


//Application code ends here

var casos = {
 'p' : require('./app/playazo/p.js')(app),
};

var server = app.listen(8080, function () {

  var host = server.address().address;
  var port = server.address().port;

  console.log('playazo app listening at http://%s:%s', host, port);

});

